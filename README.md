# renaldBezet_2_28102020



1/ Partie initialisation du projet:
1.1choix gitlab: 
interface agréable
Documentations importantes
un compte github a été créé pour pouvoir voir des projets et avoir un minimum de connaissance pour stage/job.
Possibilité de découvrir le développement en continue avec le CI/CD, peut être un plus pour la formation.
Prévu de s'intéresser et suivre cours OCR et Tuto sur les containers et Kubernete.

1.2 initialisation avec repositiory existant.
 exemple :
$git clone https://gitlab.com/name/myfirstproject.git
				$cd myfirstproject
				$touch README.md
				$git add README.md
				$git commit -m "add README"
				$git push -u origin master

2/Le projet
2.1 A partir des maquettes Desktop et Iphone8
 travail d'analyse des maquettes pour définir le balisage et les composants css.
2.2 définition de l'arborescence
2.2.1 Bezetrenald_2_28102020 ( date de début de projet)
			|--index.html ( convention pour déploiement)
			|--assets
			|	|--style.css
			|	|--components
			    |	|--button.css   -->button/ navabar/ tag/ icon
			    |	|--grid.css     -->organisation générale de la page et de ses éléments
			    |	|--card.css     --> card-city/ card-pop / card-activity
			    |--images
2.2.2 Le choix de faire plusieurs fichiers css
 Cela permet de travailler sur les composants identifiés par branche.
  Les avantages : assurer une stabilité du code, pouvoir faire plusieurs versionning et de cibler les modifications nécessaires.
2.2.3 Le nommage des fichiers
 Choix de noms courants dans le développement par rapport aux fonctionnalités développées.
 Possibilités de pouvoir réutilisés ces composants dans d'autres projets.

2.3 le développement 
 2.3.1 le choix du grid
  Dans l'optique de la formation, test de cette technologie sur découverte de l'intégration web  sachant que d'il y aura d'autres projet pour faire du flexbox.
  Pour le desktop, le grid est assez commode pour organiser sa page web, pour les media queries, il est nécessaires de repenser son organisation, ce qui peut être chronophage. 
  De nombreux choix d'unité et de mélange d'unité sont possible ( grid: 10px 1fr auto) sauf que le 3WC ne valide pas (encore) cette pratique.
 2.3.2 l'index reprend le balisage de la maquette
 2.3.4 Le css reprend le grid pour le dessin des composants
  2.3.4.1 le grid permet l'organisation de la page
  2.3.4.2 le button dessine la partie search avec différents "buttons" dont un input
  2.3.4.3 le card dessine les différentes cards.

